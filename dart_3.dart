import 'dart:io';

class Animal {
  void showColor(String color) {
    print(color);
  }

  void showName(String name) {
    print(name);
  }

  void showOwener(String owner) {
    print(owner);
  }

  void showAge(int age) {
    print(age);
  }

  void eat() {
    print("nom nom!");
  }

  void walk() {
    print("I love walking with my Owener.");
  }

  void sleep() {
    print("I like sleeping so much");
  }
}

class Dog extends Animal {
  void showPet(String pet) {
    print(pet);
  }

  void showBreed(String breed) {
    print(breed);
  }

  void bark() {
    print("Dog is bark");
  }

  void sing() {
    print("Dog Sing a Song feel good");
  }
}

class Cat extends Animal {
  void showPet(String pet) {
    print(pet);
  }

  void showWeight(int weight) {
    print(weight);
  }

  void meow() {
    print("Cat is sing Meow Meow Meow");
  }
}

void main() {
  var dog = Dog();
  dog.showPet("(Dog)");
  dog.showBreed("Breed = Retriever");
  dog.showColor("Color = Cream");
  dog.showName("Name = ViVi");
  dog.showOwener("Owener = James");
  dog.showAge(6);
  dog.bark();
  dog.sing();
  dog.eat();
  dog.walk();
  dog.sleep();

  print("-------------------------------------------");

var cat = Cat();
  cat.showPet("(Cat)");
  cat.showWeight(45);
  cat.showColor("Color = White");
  cat.showName("Name = PiePie");
  cat.showOwener("Owener = Nudee");
  cat.showAge(4);
  cat.meow();
  dog.eat();
  dog.walk();
  dog.sleep();

  print("-------------------------------------------");

  var animal = Animal();
  print("(Animals)");
  animal.showColor("Color = puzzle");
  animal.showName("Name = JiJi");
  animal.showOwener("Owener = I don't know");
  dog.eat();
  dog.walk();
  dog.sleep();
}
